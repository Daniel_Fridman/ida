import json
import Sentence

class Segment:
    # A segment is a full message that is later being splitted into multiple sentences
    all_sentences = []
    # type = sentence

    def __init__(self, segment, Id):
        # initiate the segment with the full text and an identifier
        if Id == 0:
            Segment.all_sentences = []
        self.sentences = []
        self.Full_Segment = segment
        self.Id = Id

    def __str__(self):
        return self.Full_Segment

    def addSentence(self, sentence):
        # adds sentences into this segment
        self.sentences.append(sentence)
        Segment.all_sentences.append(sentence)

    def printDetails(self):
        print("\nSegment (Id =",self.Id,"): ",self.Full_Segment)
        print("JSON: ",self.to_json())
        for i in self.sentences:
            print(i)


    def assign_ranks_by_index(self,i,rank,c):
        """Assigns by index and not by segmentation"""
        Segment.all_sentences[i].setRanks(rank)
        Segment.all_sentences[i].setCategory(c)
        return Segment.all_sentences

    def print_all_sentences(self):
        """Prints all sentences that exist (in this scope). No dependency on segment"""
        for item in Segment.all_sentences:
            print(item)

    def get_children_str(self):
        # get all of the children's json representation
        results = []
        for child in self.sentences:
            results.append(child.toJSON)
        return results

    def toJSON(self):
        # returns a json representation of this segment
        dic_obj = {"id": self.Id,
                   "full_segment": self.Full_Segment,
                   "child_count": str(len(self.sentences)),
                   "children": self.sentences
                   }
        return json.dumps(dic_obj,cls=SentenceEncoder)

    def __repr__(self):
        return str(self.toJSON())

    def get_all_sentences(self):
        """Returns all sentences with no dependency on segments.
            Access by index.
        """
        sentences=[]
        for sentence in Segment.all_sentences:
            sentences.append(sentence.content)
        return sentences

class SentenceEncoder(json.JSONEncoder):
    # helper class to cast segment into json
    def default(self, obj):
        if isinstance(obj, Sentence.Sentence):
            return {
            "id": obj.Id,
            "content":obj.content,
            "ranks":obj.ranks.tolist(),
            "category": obj.category,
                 }
        return json.JSONEncoder.default(self, obj)