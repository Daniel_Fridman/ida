import sys
import spacy

sys.path.append("..")
from dataset_creator import RedditClient
from tkinter import * # Tkinter is a Python binding to the Tk GUI toolkit.
import csv
import calendar
import time


# helper class to hold entry data
class InfoHolder:
    rank = 0
    description = ""

    def __init__(self, desc, rank):
        self.rank = rank
        self.description = desc     # content


# helper class to hold all entries as well as some constants
class DataRanker:
    right_arrow = 39
    down_arrow = 40
    change_reddit_user = 82 # R
    left_arrow = 37
    up_arrow = 38
    save = 13 # enter
    current_index = 0
    infos = [] # sentences


dr = DataRanker()
RanksLabels = ["Nothing", "(self) Opinions", "Environment", "(self) Facts"]  # labels by index
nlp = spacy.load('en_core_web_sm')  # loads NLP library


def set_labels():
    # updates the label to reflect current texts (RT view of sentence & rank)
    try:
        info_str.set(dr.infos[dr.current_index].description)
        rank_str.set(RanksLabels[dr.infos[dr.current_index].rank])
    except TclError:
        info_str.set(dr.infos[dr.current_index].description.encode('ascii', 'namereplace'))
        rank_str.set(RanksLabels[dr.infos[dr.current_index].rank])


def update_text(text):
    # split a message to sentences then save it as an entry to be processed
    doc = nlp(text)
    for sen in doc.sents:
        dr.infos.append(InfoHolder(sen.text, 0))

    set_labels()


def extract_from_reddit(name):
    # given a user name or a subreddit this will loads all of the messages
    client = RedditClient.RedditClient()
    dr.infos = []
    dr.current_index = 0

    if name.startswith('r/'):  # if it starts with r/ it is a subreddit
        subreddit = client.subreddit(name[2:])
        for submission in subreddit.hot(limit=32):
            if submission.is_self and not submission.stickied:
                # is_self - Whether or not the submission is a selfpost.
                # stickied - is it sticky?
                update_text(submission.selftext)
    else:  # otherwise - a user
        comments = client.get_comments(user=name, max_limit=None)
        for c in comments:
            update_text(c.body)


# prepare GUI
root = Tk()
info_str = StringVar() # StringVar() It's used so that you can easily monitor changes to tkinter variables if they occur
rank_str = StringVar()
info_label = Label(root, textvariable=info_str, relief=FLAT, wraplength=600, font=("David", 20))
rank_label = Label(root, textvariable=rank_str, relief=FLAT, font=("Ariel", 15))

# Label ( master, option, ... ) - This widget implements a display box where you can place text or images.
# The text displayed by this widget can be updated at any time you want.

# update text with initial one sentence tutorial
update_text("click r to start")

info_label.pack()
rank_label.pack()

# widget.pack( pack_options ) - This geometry manager organizes widgets in blocks before placing them in the parent widget.


# event handler for key precess
def key(event):
    if event.keycode == dr.right_arrow:  # increase index of category of current sentence
        dr.infos[dr.current_index].rank = min(dr.infos[dr.current_index].rank + 1, 3)
        set_labels()

    if event.keycode == dr.left_arrow:  # decrease index of category of current sentence
        dr.infos[dr.current_index].rank = max(dr.infos[dr.current_index].rank - 1, 0)

        set_labels()

    if event.keycode == dr.down_arrow:  # go to the next sentence
        dr.current_index = min(len(dr.infos) - 1, dr.current_index + 1)
        set_labels()

    if event.keycode == dr.up_arrow:  # go to the previous sentence
        dr.current_index = max(0, dr.current_index - 1)
        set_labels()

    if event.keycode == dr.save:  # save all the data into a file with unique name
        # Save format: description , rank
        ts = calendar.timegm(time.gmtime())
        f = open(str(ts) + '_data.csv', 'w', newline='')
        w = csv.writer(f, delimiter=',')
        for x in dr.infos:
            try:
                w.writerow([x.description, x.rank])
            except UnicodeEncodeError:
                w.writerow([x.description.encode('ascii', 'namereplace'), x.rank])

        f.close()
        PopupWindow(root)   # change source

    if event.keycode == dr.change_reddit_user:
        PopupWindow(root)


class PopupWindow:
    # a popup window that will request the reddit username / subreddit
    def __init__(self, master):
        top = self.top = Toplevel(master)   # Tk
        self.l = Label(top, text="Enter new reddit username")
        self.l.pack()
        self.e = Entry(top)     # accept single-line text strings from a user.
        self.e.pack()
        self.b = Button(top, text='Ok (Will get stuck please wait)', command=self.cleanup)
        self.b.pack()

    def cleanup(self):
        value = self.e.get()
        extract_from_reddit(value)
        self.top.destroy()


frame = Frame(root, width=690, height=200)  # prepare the GUI window
frame.focus_set()
frame.bind("<Key>", key)    # bind to event handler
frame.pack()

root.mainloop()  # execute GUI
