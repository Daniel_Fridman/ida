from flask import Flask, request
import predictor
import word2vec
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

print("loading GloVe (Word2Vev)...")
w2v = word2vec.word2vec()
print("GloVe is loaded successfully.")

# api route for requests of everything
@app.route('/api', methods=['POST'])
def api():
    try:
        json_object = request.get_json()  # very strict json - {"texts" : ["text1", "text2"]}
        if list(json_object.keys())[0] == 'texts':
            return str(predictor.predict_sentences(json_object['texts']))
        elif list(json_object.keys())[0] == 'search':
            top = json_object['top']
            w2v.setTop(top)
            return w2v.find_similar_words(json_object['search'])

    except Exception as e:
        return '{error: ' + str(e) + '}'


app.run(debug=False, port=5000)  # runs the app in debug mode on port 5000
