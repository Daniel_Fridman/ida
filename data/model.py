from __future__ import print_function
import pandas as pd
import os
import numpy as np
from jsonpickle import json
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from tensorflow.keras.layers import Dense, Input, GlobalMaxPooling1D
from tensorflow.keras.layers import Conv1D, MaxPooling1D, Embedding
from tensorflow.keras.models import Model
from tensorflow.keras.initializers import Constant
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import seaborn as sns


BASE_DIR = ''
GLOVE_DIR = os.path.join(BASE_DIR, 'glove.6B')
TEXT_DATA_DIR = os.path.join(BASE_DIR, 'labeled_stable')
MAX_SEQUENCE_LENGTH = 1000
MAX_NUM_WORDS = 20000
EMBEDDING_DIM = 100
VALIDATION_SPLIT = 0.2

# first, build index mapping words in the embeddings set
# to their embedding vector
print('Processing text dataset')

texts = []  # list of text samples
labels = []  # list of those samples' labels

path = os.path.join(TEXT_DATA_DIR, "")
if os.path.isdir(path):  # loads all labeled data
    for fname in sorted(os.listdir(path)):
        fpath = os.path.join(path, fname)
        data = pd.read_csv(fpath, engine='python')
        data.columns = ["Sentence", "Rank"]

        for i in range(0, data.index.size):
            sen = (data.loc[i, "Sentence"])
            rank = (data.loc[i, "Rank"])
            texts.append(sen)
            labels.append(rank)

print('Indexing word vectors.')

embeddings_index = {}
with open(os.path.join(GLOVE_DIR, 'glove.6B.100d.txt'), encoding="utf8") as f:
    for line in f:  # Loads Glove's Word2Vec
        word, coefs = line.split(maxsplit=1)
        coefs = np.fromstring(coefs, 'f', sep=' ')
        embeddings_index[word] = coefs

print('Found %s word vectors.' % len(embeddings_index))

print('Found %s texts.' % len(texts))

tokenizer = Tokenizer(num_words=MAX_NUM_WORDS)
tokenizer.fit_on_texts(texts)  # extract tokens from all the labeled data
sequences = tokenizer.texts_to_sequences(texts)  # convert texts to sequences
tokenizer_json = tokenizer.to_json()  # converts to json so we could....
with open('tokenizer.json', 'w', encoding='utf-8') as f:
    f.write(json.dumps(tokenizer_json))  # ....save the file for later use
word_index = tokenizer.word_index # word_index: A dictionary of words and their uniquely assigned integers.
print('Found %s unique tokens.' % len(word_index))

data = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH)  # padding all text so they are all uniform size

labels = to_categorical(np.asarray(labels))
# converting labels into a structure the model can understand:
# before: [0, 2, ...]
# after:
# [[1 0 0 0] - label: 0
#  [0 0 1 0] - label: 2
#        ...]

print('Shape of data tensor:', data.shape)
print('Shape of label tensor:', labels.shape)
print('Mr. lavebls', labels)
# split the data into a training set and a validation set
indices = np.arange(data.shape[0]) # Returns evenly spaced values within a given interval (indices of samples in data)
np.random.shuffle(indices)
data = data[indices]
labels = labels[indices]
num_validation_samples = int(VALIDATION_SPLIT * data.shape[0])

x_train = data[:-num_validation_samples]
y_train = labels[:-num_validation_samples]
x_val = data[-num_validation_samples:]
y_val = labels[-num_validation_samples:]

print('Preparing embedding matrix.')

# prepare embedding matrix
num_words = min(MAX_NUM_WORDS, len(word_index) + 1)
embedding_matrix = np.zeros((num_words, EMBEDDING_DIM)) # matrix: words X vectors
for word, i in word_index.items():  # i = uniquely assigned integer (token)
    if i >= MAX_NUM_WORDS:
        # no need considering words with higher token then the limitation.
        continue
    embedding_vector = embeddings_index.get(word)
    # embeddings_index is full word2vec dictionary.
    # embedding_vector is this dictionary filtered and ordered by our tokens and limitations.
    if embedding_vector is not None:
        # words not found in embedding index will be all-zeros.
        embedding_matrix[i] = embedding_vector

# load pre-trained word embeddings into an Embedding layer.
# note that we set trainable = False so as to keep the embeddings fixed.
embedding_layer = Embedding(num_words,
                            EMBEDDING_DIM,
                            embeddings_initializer=Constant(embedding_matrix),
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=False)

print('Training model.')


#The focal loss is designed to address class imbalance by down-weighting inliers (easy examples)
#such that their contribution to the total loss is small even if their number is large.
#It focuses on training a sparse set of hard examples.
def focal_loss(gamma=2., alpha=4.):

    gamma = float(gamma)
    alpha = float(alpha)

    def focal_loss_fixed(y_true, y_pred):
        # Focal loss for multi-classification
        # Note: y_pred is probability after softmax while y_true is the actual class.
        # FL(p_t)=-alpha(1-p_t)^{gamma}ln(p_t)
        # gradient is d(Fl)/d(p_t)
        # d(Fl)/d(p_t) * [p_t(1-p_t)] = d(Fl)/d(x)

        epsilon = 1.e-9
        y_true = tf.convert_to_tensor(y_true, tf.float32)
        y_pred = tf.convert_to_tensor(y_pred, tf.float32)

        model_out = tf.add(y_pred, epsilon)     # sum
        ce = tf.multiply(y_true, -tf.math.log(model_out))
        weight = tf.multiply(y_true, tf.pow(tf.subtract(1., model_out), gamma))
        fl = tf.multiply(alpha, tf.multiply(weight, ce))
        reduced_fl = tf.reduce_max(fl, axis=1)
        return tf.reduce_mean(reduced_fl)
    return focal_loss_fixed


# train a 1D convnet with global maxpooling
sequence_input = Input(shape=(MAX_SEQUENCE_LENGTH,), dtype='int32')
embedded_sequences = embedding_layer(sequence_input)
x = Conv1D(128, 5, activation='relu')(embedded_sequences)
x = MaxPooling1D(5)(x)
x = Conv1D(128, 5, activation='relu')(x)
x = MaxPooling1D(5)(x)
x = Conv1D(128, 5, activation='relu')(x)
x = GlobalMaxPooling1D()(x)
x = Dense(128, activation='relu')(x)
preds = Dense(4, activation='softmax')(x)

model = Model(sequence_input, preds)
model.compile(loss=focal_loss(alpha=1),
              optimizer='rmsprop',
              metrics=['acc']) # compiles the model and prepare it for training
history = model.fit(x_train, y_train,
                    batch_size=128,
                    epochs=32,
                    validation_data=(x_val, y_val)) # train the model
model.save('model.hdf5') # save model
y_pred = model.predict(x_val) # generate predictions from validation set
cm = confusion_matrix(np.argmax(y_val, axis=1), np.argmax(y_pred, axis=1), normalize='true') # make confusion matrix from that

ax= plt.subplot()
sns.heatmap(cm, annot=True, ax = ax); #annot=True to annotate cells

print(cm)

plt.show() # show confusion matrix
