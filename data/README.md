# IDA\Data

MGP - MovieGroupProcess:
```
mgp.py - core
mgp.topics - model
mgp_train.py - trainer
```

CNN - Convolutional Neural Network:
```
model.py - core
model.hdf5 - model
tokenizer.json - stored tokens for words
```


Runnables
```
mgp_train.py
model.py
dataStabilizator.py
```