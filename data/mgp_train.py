import os
import pandas as pd
from mgp import MovieGroupProcess


def compute_V(texts):
    """Return length of a set of all words (items in all student's lists)"""
    V = set()
    # set() won't include duplications.
    for text in texts:
        for word in text:
            V.add(word)
    return len(V)

def getData(path):
    texts =[]
    if os.path.isdir(path):
        for fname in sorted(os.listdir(path)):
            fpath = os.path.join(path, fname)
            data = pd.read_csv(fpath, engine='python')
            for i in range(0, data.index.size):
                sen = str((data.loc[i, data.columns[0]]).strip().encode('ascii', 'namereplace'))
                if len(sen) > 5:
                    texts.append(sen)
    return texts


texts = getData('labeled')
texts = [text.split() for text in texts]
V = compute_V(texts)    # amount of unique words in data (vocab size).
mgp = MovieGroupProcess(K=5, n_iters=100, alpha=0.2, beta=0.01)
y = mgp.fit(texts, V)
mgp.to_file("mgp.topics")
