import os
import pandas as pd
import csv

BASE_DIR = ''
TEXT_DATA_DIR = os.path.join(BASE_DIR, 'labeled')
path = os.path.join(TEXT_DATA_DIR, "")



class dataStabiliztor():

    def __init__(self):
        self.ranks = {}
        self.ranks[0] = [] #Nothing
        self.ranks[1] = [] #Opininos
        self.ranks[2] = [] #Environment
        self.ranks[3] = [] #Facts

    def loadData(self):
        if os.path.isdir(path):
            for fname in sorted(os.listdir(path)):
                fpath = os.path.join(path, fname)
                data = pd.read_csv(fpath, engine='python')

                for i in range(0, data.index.size):
                    sen = (data.loc[i, data.columns[0]])
                    rank = (data.loc[i, data.columns[1]])
                    self.organizer(rank, sen)
        print("Data Received Successfully")

    def organizer(self, rank, sent):
        # adds sentences to the relevant rank list.
        self.ranks[rank].append(sent)

    def getShortestArray(self):
        minLen = None
        for arr in self.ranks.values():
            if minLen == None:
                minLen=len(arr)
            else:
                minLen=min(minLen,len(arr))
        print("Shortest rank is: ", minLen)
        return minLen

    def trimAllArrays(self, trimValue, differenceFactor):
        # trim all arrays by sum(trimvalue + some % of it)
        # differenceFactor => -1 to 1
        if abs(differenceFactor) > 1:
            print("Bad differenceFactor")
        else:
            for key in self.ranks.keys():
                tmp = int(trimValue + differenceFactor * trimValue)
                upperLimit = min(tmp, len(self.ranks[key]))
                self.ranks[key] = self.ranks[key][:upperLimit]
            print("Lists are Trimmed")

    def getTotalAmount(self):
        sum = 0
        for arr in self.ranks.values():
            sum = sum + len(arr)
        return sum

    def printLengthes(self):
        print("-----------------Lengths------------------------")
        for key in self.ranks.keys():
            if key == 0: print("Nothing Length:")
            if key == 1: print("Opinions Length:")
            if key == 2: print("Environment Length:")
            if key == 3: print("Facts Length:")
            print(len(self.ranks[key]))
        print("-----------------------------------------------")

    def printContents(self):
        print("-----------------Content------------------------")
        for key in self.ranks.keys():
            if key == 0: print("Nothing Contains:")
            if key == 1: print("Opinions Contains:")
            if key == 2: print("Environment Contains:")
            if key == 3: print("Facts Contains:")
            print(self.ranks[key])
        print("-----------------------------------------------")

    def save(self):
        f = open('labeled_stable/stabilized_data.csv', 'w', newline='')
        w = csv.writer(f, delimiter=',')
        for key in self.ranks.keys():
            for sent in self.ranks[key]:
                try:
                    w.writerow([sent, key])
                except UnicodeEncodeError:
                    w.writerow([sent.encode('ascii', 'namereplace'), key])
        print("Stabilized version of data is saved at stabilized_data.csv")


ds = dataStabiliztor()
ds.loadData()
ds.printLengthes()
print("Total size: ", ds.getTotalAmount())
s = ds.getShortestArray()
ds.trimAllArrays(s, 0.2)
ds.printLengthes()
print("New total size: ", ds.getTotalAmount())
ds.save()
