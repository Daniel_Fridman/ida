class Sentence:

    def __init__(self, sentence, ranks, i, segment=-1):
        # construct a sentence with a parent segment
        self.content = sentence
        self.ranks = ranks
        self.Id = i
        self.segment = segment
        self.category = -1

    def setRanks(self, r):
        # sets the confidence of all categories
        self.ranks = r

    def getSegment(self):
        # returns the parent segment
        return self.segment

    def setCategory(self, cat):
        # sets the mgp category
        self.category = int(cat)

    def __str__(self):
        return self.content

