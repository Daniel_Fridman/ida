import numpy as np
from numpy.linalg import norm
import json
import os


class word2vec:
    def __init__(self):
        # Loads Word2Vec Glove into dictionary

        BASE_DIR = 'data'
        GLOVE_DIR = os.path.join(BASE_DIR, 'glove.6B')

        self.embeddings_index = {}
        with open(os.path.join(GLOVE_DIR, 'glove.6B.100d.txt'), encoding="utf8") as f:
            for line in f:
                word, coefs = line.split(maxsplit=1)
                coefs = np.fromstring(coefs, 'f', sep=' ')
                self.embeddings_index[word] = coefs
        self.words = self.embeddings_index.keys()

    def setTop(self, top):
        self.n = top

    def cosine(self, v1, v2):
        #measure cosine similarity between 2 vectors

        if norm(v1) > 0 and norm(v2) > 0:
            return np.dot(v1, v2) / (norm(v1) * norm(v2))
        else:
            return 0.0

    def spacy_closest(self, vec_to_check):
        #checks top n closest words to given vec(word)

        tmp = sorted(self.words,
                      key=lambda x: self.cosine(vec_to_check, self.embeddings_index[x]),
                      reverse=True)[:self.n]
        return self.toJSON(tmp)

    def find_similar_words(self, word):
        # returns similar words or, if word is not in the GloVe, [].
        try:
            res = self.spacy_closest(self.vec(word.lower()))
        except:
            res = self.toJSON([])
        return res

    def vec(self, word):
        #word to vec
        return self.embeddings_index[word]

    def toJSON(self, neighborList):
        dic_obj = {"neighbors": neighborList}
        return json.dumps(dic_obj)
