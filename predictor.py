import json
from keras_preprocessing.text import tokenizer_from_json
import spacy

from tensorflow.keras.models import load_model
from keras_preprocessing.sequence import pad_sequences

import Sentence
import Segment

MAX_SEQUENCE_LENGTH = 1000


model = load_model('data/model.hdf5', compile=False)  # loading the model for prediction purpose only.
nlp = spacy.load("en_core_web_sm")  # load the NLP library spacy
data = None
tokenizer = None
with open('data/tokenizer.json') as f:  # loads the word tokenizer used in the model training
    data = json.load(f)
    tokenizer = tokenizer_from_json(data)


def printSegments(segs):
    for i in range(len(segs)):
        segs[i].printDetails()


def predict_sentences(comments):
    """
    Given a list of strings (messages) this will return an array of processed segments.
    """
    segments = []
    # generates a list of segments from the messages. (split comments into sentences)
    for id, comment in enumerate(comments):
        newSegment = Segment.Segment(comment, id)
        doc = nlp(comment)
        i = 0   # sentence id
        for sen in doc.sents:
            if len(sen) > 5:
                newSentence = Sentence.Sentence(sen.text, -1, i, newSegment)    # Sentence (txt, ranks, sentId, segmentId)
                newSegment.addSentence(newSentence)
                i += 1
        segments.append(newSegment)
    # prepares data to pass through model
    sequences = tokenizer.texts_to_sequences(Segment.Segment.get_all_sentences(segments))     # tokenization of all texts
    padded = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH)   # padding
    # predicts the personal categories of everything
    predictions = model.predict(padded)
    # predicts the mgp categories of everything
    ct = categorizeSentences(Segment.Segment.all_sentences)
    # prepares data to send back
    for i in range(0, len(Segment.Segment.all_sentences)):
        p = predictions[i]
        c = ct[i]
        Segment.Segment.assign_ranks_by_index(segments, i, p, c)  # assigns categories as well.
    return segments     # sends back list of all processed segments.


from sys import path
path.append("\\data")
from data.mgp import MovieGroupProcess

# given sentences return their mgp categories
def categorizeSentences(sentences):
    texts = [sent.content.split() for sent in sentences]
    # texts = [[word0 of sent0, word1 of sent0...],[word0 of sent1, word1 of sent1....]]
    # text is sentence.
    lables = []
    reloaded = MovieGroupProcess.from_file("data\mgp.topics")
    for text in texts:
        tmp = reloaded.choose_best_label(text)
        lables.append(tmp[0])
    return lables
