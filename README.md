# IDA

Information Detection & Analysis

Requirements:
```
pip install praw==7.1.0
pip install numpy==1.19
pip install tensorflow==2.0.0
pip install keras 2.3.1
pip install scikit-learn==1.4.1
pip install spacy==2.2.4
python -m spacy download en_core_web_sm
pip install flask==1.1.2
```

Run app.py